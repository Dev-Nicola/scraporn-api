# How to run Scraporn API

## Prerequisites

- Install Xampp (pour PHP et MySQL)
- Install Composer

## How to run the api
- Clone this repo
- Inside the repo : first run ```composer install```
- Make a copy of the ".env.example" and rename it ".env"
- Inside it, insert the right variables for your database connection
- Then generate a secret key for JWT with ```php artisan jwt:secret```
- Migrate the database with ```php artisan migrate```
- Type ```php -S localhost:8000 -t public``` in your terminal to run the api.

# How to generate API docs

## Swagger (Open API)

In the command line, inside the root of the project run the following command:

`./vendor/bin/openapi --format json --output ./public/swagger/swagger.json ./swagger/swagger.php app`

This will extract all the annotations in the app/ folder then it will format it into a json file and save it inside the swagger/ folder.

# Best Practice

## Git Branch Naming Convention

__Git Flow__ is recommended for development of the api.
The default naming will be used:
- master
- development
- feature
- release
- bugfix

### Initialize Git Flow

Run ```git flow init``` to initialize git flow into your local git environment.

Example of how to create a new feature in your program: 
```git flow feature start feature_branch```

## Conventional Commits

__Commitizen__ is __strongly__ recommended in order to format your commits and have a consistent repository.

### Initialiaze Commitizen

Run the following command ```git cz``` to create a commit.

# Lumen

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
