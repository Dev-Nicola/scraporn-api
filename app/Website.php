<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Website extends Model
{

    /**
     * @OA\Property(
     *      property="name",
     *      type="string",
     *      description="Website's name"
     * ),
     * @OA\Property(
     *      property="url",
     *      type="string",
     *      description="Website's url"
     * ),
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
