<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use OpenApi\Annotations as OA;

use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema()
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     * @OA\Property(
     *      property="username",
     *      type="string",
     *      description="Username"
     * ),
     * @OA\Property(
     *      property="email",
     *      type="string",
     *      description="Email"
     * ),
     * @OA\Property(
     *      property="firstname",
     *      type="string",
     *      description="Firstname",
     *      nullable=true
     * ),
     * @OA\Property(
     *      property="lastname",
     *      type="string",
     *      description="Lastname",
     *      nullable=true
     * ),
     * @OA\Property(
     *      property="pornhubProfile",
     *      type="string",
     *      description="Pornhub profile",
     *      nullable=true
     * ),
     * @OA\Property(
     *      property="twitterUsername",
     *      type="string",
     *      description="Twitter username",
     *      nullable=true
     * ),
     * @OA\Property(
     *      property="description",
     *      type="string",
     *      description="Description",
     *      nullable=true
     * ),
     * @OA\Property(
     *      property="avatar",
     *      type="string",
     *      description="Avatar",
     *      nullable=true
     * )
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'firstname',
        'lastname',
        'pornhubProfile',
        'twitterUsername',
        'description',
        'avatar'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
