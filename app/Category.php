<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     * @OA\Property(
     *      property="name",
     *      type="string",
     *      description="Category's name"
     * ),
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
