<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Tag extends Model
{

    /**
     * The attributes that are mass assignable.
     * @OA\Property(
     *      property="name",
     *      type="string",
     *      description="Tag's name"
     * ),
     * @var array
     */
    protected $fillable = [
        'name',
        'website_id',
        'category_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
