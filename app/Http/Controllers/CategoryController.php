<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Instantiate a new WebsiteController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * @OA\Post(
     *      path="/categories",
     *      tags={"Category"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(@OA\Items(ref="#/components/schemas/Category"))
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="category created"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while creating a new category.",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while creating a new category."
     *              )
     *          )
     *      )
     * )
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:categories'
        ]);

        $category = Category::create($request->all());

        return response()->json($category, 201);
    }

    /**
     * @OA\Get(
     *      path="/categories/{id}",
     *      tags={"Category"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The category id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one tag",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Category"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching categories.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching categories.")
     *          )
     *      )
     * )
    */
    public function showOneCategory($id)
    {
        try {
            $category = Category::findOrFail($id);
            return response()->json(['category' => $category], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Category not found!'], 404);
        }
    }

        /**
     * @OA\Get(
     *      path="/categories",
     *      tags={"Category"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all categories",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Category"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching categories.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching categories.")
     *          )
     *      )
     * )
     */
    public function showAllCategories()
    {
        return response()->json(['categories' => Category::all()], 200);
    }

        /**
     * @OA\Put(
     *      path="/categories/{id}",
     *      tags={"Category"},
     *      description="Update one category.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The category id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This category has been updated.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This category has been updated.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The category does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The category does not exist.")
     *          )
     *      ),
     *      @OA\RequestBody(
     *         description="Update a category.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Category")
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        try {
            $category = Category::findOrFail($id);
            $category->update($request->all());
            return response()->json(['category' => $category, 'Message' => 'Category updated!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Category not found!'], 404);
        }
    }

        /**
     * @OA\Delete(
     *      path="/categories/{id}",
     *      tags={"Category"},
     *      description="Delete one category.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The category id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This category has been deleted.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This category has been deleted.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The category does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The category does not exist.")
     *          )
     *      )
     * )
    */
    public function delete($id)
    {
        Category::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
