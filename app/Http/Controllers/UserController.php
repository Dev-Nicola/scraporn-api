<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @OA\Get(
     *      path="/users/{id}",
     *      tags={"User"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The user id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one user",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/User"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching users.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching users.")
     *          )
     *      )
     * )
    */
    public function showOneUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {
            return reponse()->json(['message' => 'user not found!'], 404);
        }
    }

    public function showOneUserByUsername($username)
    {
        try {
            $user = User::where('username', $username)->first();

            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {
            return reponse()->json(['message' => 'user not found!'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/users",
     *      tags={"User"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all users",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/User"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching users.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching users.")
     *          )
     *      )
     * )
     */
    public function showAllUsers()
    {
        return response()->json(['users' => User::all()], 200);
    }

    /**
     * @OA\Put(
     *      path="/users/{id}",
     *      tags={"User"},
     *      description="Update one user.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The user id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This user has been updated.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This user has been updated.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The user does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The user does not exist.")
     *          )
     *      ),
     *      @OA\RequestBody(
     *         description="Update a user.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());
            return response()->json(['user' => $user, 'Message' => 'User updated!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'User not found!'], 404);
        }
    }

     /**
     * @OA\Delete(
     *      path="/users/{id}",
     *      tags={"User"},
     *      description="Delete one user.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The user id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This user has been deleted.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This user has been deleted.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The user does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The user does not exist.")
     *          )
     *      )
     * )
    */
    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
