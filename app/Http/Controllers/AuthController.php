<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *      path="/register",
     *      tags={"Register"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="username",
     *                  type="string",
     *                  description="Username"
     *              ),
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  description="Email"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  description="Password"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="User created"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Problem user registration",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while creating a new user."
     *              )
     *          )
     *      )
     * )
     * Store a new user.
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        try {
            $user = new User;
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            
            $user->save();

            return response()->json(['user' => $user, 'message' => 'User created'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }

    
     /**
     * @OA\Post(
     *      path="/login",
     *      tags={"Login"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  description="Email"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  description="Password"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="User logged in"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Problem user login",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while logging in a user."
     *              )
     *          )
     *      )
     * )
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $user_id = User::select('id')->where('email', $request['email'])->first();
        $user_id = json_decode($user_id, true);

        $token = $this->respondWithToken($token, true);
        $token = array_merge($token, $user_id);

        return response()->json($token, 200);
    }
}
