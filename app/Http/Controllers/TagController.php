<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Instantiate a new WebsiteController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @OA\Post(
     *      path="/tags",
     *      tags={"Tag"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(@OA\Items(ref="#/components/schemas/Tag"))
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Tag created"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while creating a new tag.",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while creating a new tag."
     *              )
     *          )
     *      )
     * )
     */
    public function create(Request $request)
    {
        $tag = Tag::create($request->all());

        return response()->json(['tag' => $tag], 201);
    }

    /**
     * @OA\Get(
     *      path="/tags/{id}",
     *      tags={"Tag"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The tag id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one tag",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Tag"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching tags.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching tags.")
     *          )
     *      )
     * )
    */
    public function showOneTag($id)
    {
        try {
            $tag = Tag::findOrFail($id);
            return response()->json(['tag' => $tag], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Tag not found'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/tags",
     *      tags={"Tag"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all tags",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Tag"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching tags.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching tags.")
     *          )
     *      )
     * )
     */
    public function showAllTags()
    {
        return response()->json(['tags' => Tag::all()], 200);
    }

    /**
     * @OA\Put(
     *      path="/tags/{id}",
     *      tags={"Tag"},
     *      description="Update one tag.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The tag id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This tag has been updated.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This tag has been updated.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The tag does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The tag does not exist.")
     *          )
     *      ),
     *      @OA\RequestBody(
     *         description="Update a tag.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Tag")
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        try {
            $tag = Tag::findOrFail($id);
            $tag->update($request->all());
            return response()->json(['tag' => $tag, "Message" => "Update successful!"], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Tag not found'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/tags/{id}",
     *      tags={"Tag"},
     *      description="Delete one tag.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The tag id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This tag has been deleted.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This tag has been deleted.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The tag does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The tag does not exist.")
     *          )
     *      )
     * )
    */
    public function delete($id)
    {
        Tag::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
