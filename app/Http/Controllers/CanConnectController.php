<?php

namespace App\Http\Controllers;

class CanConnectController extends Controller
{
    public function canConnect()
    {
        return response()->json('connected');
    }
}
