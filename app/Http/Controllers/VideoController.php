<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Instantiate a new WebsiteController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @OA\Post(
     *      path="/videos",
     *      tags={"Video"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(@OA\Items(ref="#/components/schemas/Video"))
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Video added"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while creating a new video.",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while creating a new video."
     *              )
     *          )
     *      )
     * )
     */
    public function create(Request $request)
    {
        $video = Video::create($request->all());

        return response()->json($video, 201);
    }

       /**
     * @OA\Get(
     *      path="/videos/{id}",
     *      tags={"Video"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The video id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one video",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Video"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching videos.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching videos.")
     *          )
     *      )
     * )
    */
    public function showOneVideo($id)
    {
        try {
            $video = Video::findOrFail($id);
            return response()->json(['video' => $video], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Video not found'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/videos",
     *      tags={"Video"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all videos",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Video"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching videos.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching videos.")
     *          )
     *      )
     * )
     */
    public function showAllVideos()
    {
        return response()->json(['videos' => Video::all()], 200);
    }

    /**
     * @OA\Put(
     *      path="/videos/{id}",
     *      tags={"Video"},
     *      description="Update one video.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The video id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This video has been updated.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This video has been updated.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The video does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The video does not exist.")
     *          )
     *      ),
     *      @OA\RequestBody(
     *         description="Update a video.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Video")
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        try {
            $video = Video::findOrFail($id);
            $video->update($request->all());
            return response()->json(['Video' => $video, "Message" => "Video updated!"], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Video not found'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/videos/{id}",
     *      tags={"Video"},
     *      description="Delete one video.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The video id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This video has been deleted.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This video has been deleted.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The video does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The video does not exist.")
     *          )
     *      )
     * )
    */
    public function delete($id)
    {
        Video::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
