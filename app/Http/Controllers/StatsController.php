<?php

namespace App\Http\Controllers;

use App\Stats;
use Illuminate\Http\Request;

class StatsController extends Controller
{
    /**
     * Instantiate a new WebsiteController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @OA\Get(
     *      path="/statistics/{id}",
     *      tags={"Statistics"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The statistics id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one statistics",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Statistics"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching statistics.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching statistics.")
     *          )
     *      )
     * )
    */
    public function showOneStatistics($id)
    {
        return response()->json(Stats::find($id));
    }

    public function showOneStatisticsByVideoId($id)
    {
        $stats = Stats::where('video_id', '=', $id)->get();
        return response()->json(['stats' => $stats], 200);
    }

    /**
     * @OA\Get(
     *      path="/statistics",
     *      tags={"Statistics"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all statistics",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Statistics"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching statistics.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching statistics.")
     *          )
     *      )
     * )
     */
    public function showAllStatistics()
    {
        return response()->json(Stats::all());
    }
}
