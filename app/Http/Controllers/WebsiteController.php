<?php

namespace App\Http\Controllers;

use App\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    /**
     * Instantiate a new WebsiteController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @OA\Post(
     *      path="/websites",
     *      tags={"Website"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(@OA\Items(ref="#/components/schemas/Website"))
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Website created"
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while creating a new website.",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="A problem occured while creating a new website."
     *              )
     *          )
     *      )
     * )
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:websites',
            'url' => 'required|string'
        ]);

        $website = Website::create($request->all());

        return response()->json(['website' => $website, 'message' => 'Website created'], 201);
    }
    
    /**
     * @OA\Get(
     *      path="/websites/{id}",
     *      tags={"Website"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The website id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Get one website",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Website"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching websites.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching websites.")
     *          )
     *      )
     * )
    */
    public function showOneWebsite($id)
    {
        try {
            $website = Website::findOrFail($id);
            return response()->json(['website' => $website], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Website not found'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/websites",
     *      tags={"Website"},
     *      @OA\Response(
     *          response="200",
     *          description="Get all websites",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Website"))
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="A problem occured while fetching websites.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A problem occured while fetching websites.")
     *          )
     *      )
     * )
     */
    public function showAllWebsites()
    {
        return response()->json(['websites' => Website::all()], 200);
    }

    /**
     * @OA\Put(
     *      path="/websites/{id}",
     *      tags={"Website"},
     *      description="Update one website.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The website id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This website has been updated.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This website has been updated.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The website does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The website does not exist.")
     *          )
     *      ),
     *      @OA\RequestBody(
     *         description="Update a website.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Website")
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        try {
            $website = Website::findOrFail($id);
            $website->update($request->all());
            return response()->json(['website' => $website, "Message" => "Update successful!"], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Website not found'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/websites/{id}",
     *      tags={"Website"},
     *      description="Delete one website.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="The website id",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="This website has been deleted.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="This website has been deleted.")
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="The website does not exist.",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The website does not exist.")
     *          )
     *      )
     * )
    */
    public function delete($id)
    {
        Website::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
