<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Stats extends Model
{

    /**
     * The attributes that are mass assignable.
     * @OA\Property(
     *      property="like",
     *      type="integer",
     *      description="Like count"
     * ),
     * @OA\Property(
     *      property="comment",
     *      type="integer",
     *      description="Comment count"
     * ),
     * @OA\Property(
     *      property="scrapped_at",
     *      type="string",
     *      description="Date where the video has been scrapped."
     * ),
     * @var array
     */
    protected $fillable = [
        'liked', 'comment', 'scrapped_at', 'video_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
