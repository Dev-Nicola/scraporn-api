<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Video extends Model
{

    /**
     * @OA\Property(
     *      property="url",
     *      type="string",
     *      description="Video's url"
     * )
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'website_id',
        'user_id',
        'tag_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
