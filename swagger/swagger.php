<?php

use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="Scraporn API Documentation", version="0.1")
 * @OA\server(
 *      url="localhost:8000/api/v1",
 *      description="Scraporn API"
 * )
 */