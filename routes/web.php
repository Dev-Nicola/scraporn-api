<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    
    // /**
    //  * Check if the connection is established between the back and front
    //  */
    // $router->get('/can_connect', function () use ($router)->middleware('cors'); {
    //     return 'connected';
    // });

    $router->get('/can_connect', ['uses' => 'CanConnectController@canConnect']);

    // Authentification
    $router->post('/register', ['uses' => 'AuthController@register']);
    $router->post('/login', 'AuthController@login');

    $router->get('/users/{id}', ['uses' => 'UserController@showOneUser']);
    $router->get('/users/username/{username}', ['uses' => 'UserController@showOneUserByUsername']);
    $router->get('/users', ['uses' => 'UserController@showAllUsers']);
    $router->put('/users/{id}', ['uses' => 'UserController@update']);

    $router->post('/websites', ['uses' => 'WebsiteController@create']);
    $router->get('/websites/{id}', ['uses' => 'WebsiteController@showOneWebsite']);
    $router->get('/websites', ['uses' => 'WebsiteController@showAllWebsites']);
    $router->put('/websites/{id}', ['uses' => 'WebsiteController@update']);
    $router->delete('/websites/{id}', ['uses' => 'WebsiteController@delete']);

    $router->post('/categories', ['uses' => 'CategoryController@create']);
    $router->get('/categories/{id}', ['uses' => 'CategoryController@showOneCategory']);
    $router->get('/categories', ['uses' => 'CategoryController@showAllCategories']);
    $router->put('/categories/{id}', ['uses' => 'CategoryController@update']);
    $router->delete('/categories/{id}', ['uses' => 'CategoryController@delete']);

    $router->post('/videos', ['uses' => 'VideoController@create']);
    $router->get('/videos/{id}', ['uses' => 'VideoController@showOneVideo']);
    $router->get('/videos', ['uses' => 'VideoController@showAllVideos']);
    $router->put('/videos/{id}', ['uses' => 'VideoController@update']);
    $router->delete('/videos/{id}', ['uses' => 'VideoController@delete']);

    $router->post('/tags', ['uses' => 'TagController@create']);
    $router->get('/tags/{id}', ['uses' => 'TagController@showOneTag']);
    $router->get('/tags', ['uses' => 'TagController@showAllTags']);
    $router->put('/tags/{id}', ['uses' => 'TagController@update']);
    $router->delete('/tags/{id}', ['uses' => 'TagController@delete']);

    $router->get('/stats/video/{id}', ['uses' => 'StatsController@showOneStatisticsByVideoId']);
});
